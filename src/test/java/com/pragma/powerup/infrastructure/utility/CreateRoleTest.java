package com.pragma.powerup.infrastructure.utility;

import com.pragma.powerup.application.handler.impl.RoleHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CreateRoleTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RoleHandler roleHandler;

    @Test
    public void createRole() throws Exception {
        //given
        boolean isRoleCreated = roleHandler.getRoleByName("ROLE_ADMIN").isEmpty() &&
                roleHandler.getRoleByName("ROLE_CLIENT").isEmpty() &&
                roleHandler.getRoleByName("ROLE_EMPLOYEE").isEmpty() &&
                roleHandler.getRoleByName("ROLE_OWNER").isEmpty();
        //when
        new CreateRole(roleHandler).run();
        //then
        assertNotNull(isRoleCreated);
    }
}

