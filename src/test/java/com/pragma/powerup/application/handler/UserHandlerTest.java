package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.SaveUserRequestDto;
import com.pragma.powerup.application.handler.impl.UserHandler;
import com.pragma.powerup.application.mapper.IUserRequestMapper;
import com.pragma.powerup.domain.api.IUserServicePort;
import com.pragma.powerup.domain.model.UserModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserHandlerTest {

    @Spy
    private IUserServicePort userServicePort;

    @Spy
    private IUserRequestMapper userRequestMapper;

    @InjectMocks
    private UserHandler userHandler;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSaveUser() {

        // Given
        Set<String> roles = new HashSet<>();
        roles.add("ROLE_OWNER");
        SaveUserRequestDto saveUserRequestDto = new SaveUserRequestDto();
        saveUserRequestDto.setCellphone("+123456782011");
        saveUserRequestDto.setDocument("1234161");
        saveUserRequestDto.setEmail("asa@gmail.com");
        saveUserRequestDto.setName("asa");
        saveUserRequestDto.setLastName("asa");
        saveUserRequestDto.setPassword("1234");
        saveUserRequestDto.setRoles(roles);

        UserModel userModel = new UserModel();
        when(userRequestMapper.toUserModel(saveUserRequestDto)).thenReturn(userModel);

        // When
        userHandler.saveUser(saveUserRequestDto);

        // Then
        verify(userServicePort).saveUser(userModel);
    }
}
