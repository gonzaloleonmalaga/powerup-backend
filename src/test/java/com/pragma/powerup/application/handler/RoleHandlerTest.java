package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.RoleRequestDto;
import com.pragma.powerup.application.handler.impl.RoleHandler;
import com.pragma.powerup.application.mapper.IRoleRequestMapper;
import com.pragma.powerup.domain.api.IRoleServicePort;
import com.pragma.powerup.domain.model.RoleModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;

import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class RoleHandlerTest {

    @InjectMocks
    private RoleHandler roleHandler;

    @Spy
    private IRoleServicePort roleServicePort;

    @Spy
    private IRoleRequestMapper roleRequestMapper;

    @Test
    public void testSaveRole() {
        // Given
        RoleRequestDto roleRequestDto = new RoleRequestDto();
        RoleModel expectedModel = new RoleModel();
        when(roleRequestMapper.toRoleModel(roleRequestDto)).thenReturn(expectedModel);

        // When
        roleHandler.saveRole(roleRequestDto);

        // Then
        ArgumentCaptor<RoleModel> captor = ArgumentCaptor.forClass(RoleModel.class);
        verify(roleServicePort).saveRole(captor.capture());
        RoleModel actualModel = captor.getValue();
        assertEquals(expectedModel, actualModel);
    }
}