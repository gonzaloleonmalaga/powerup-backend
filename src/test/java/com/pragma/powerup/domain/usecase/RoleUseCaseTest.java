package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.spi.IRolePersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RoleUseCaseTest {

    @Mock
    private IRolePersistencePort rolePersistencePort;

    private RoleUseCase roleUseCase;

    @BeforeEach
    public void setUp() {
        roleUseCase = new RoleUseCase(rolePersistencePort);
    }

    @Test
    public void testSaveRole() {
        // Given
        RoleModel roleModel = new RoleModel("ROLE_OWNER");

        // When
        roleUseCase.saveRole(roleModel);

        // Then
        verify(rolePersistencePort).saveRole(roleModel);
    }
}
