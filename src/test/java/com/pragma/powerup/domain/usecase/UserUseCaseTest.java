package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.spi.IRolePersistencePort;
import com.pragma.powerup.domain.spi.IUserPersistencePort;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserUseCaseTest {

    @Spy
    private IUserPersistencePort userPersistencePort;

    @Spy
    private IRolePersistencePort rolePersistencePort;

    @InjectMocks
    private UserUseCase userUseCase;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSaveUserWithRoles() {
        // Given
        Set<RoleModel> roles = new HashSet<>();
        roles.add(new RoleModel("ROLE_OWNER"));
        UserModel userModel = new UserModel();
        userModel.setDocument("123456789");
        userModel.setName("John");
        userModel.setLastName("Doe");
        userModel.setEmail("johndoe@gmail.com");
        userModel.setPassword("password");
        userModel.setCellphone("555-5555");
        userModel.setRoles(roles);

        // Mocking rolePersistencePort
        RoleModel ownerRole = new RoleModel("ROLE_OWNER");
        when(rolePersistencePort.findAllByNameIn(anySet())).thenReturn(Arrays.asList(ownerRole));

        // When
        userUseCase.saveUser(userModel);

        // Then
        verify(rolePersistencePort).findAllByNameIn(roles.stream().map(RoleModel::getName).collect(Collectors.toSet()));
        verify(userPersistencePort).saveUser(userModel);
    }

    @Test
    public void testSaveUserWithoutRoles() {
        // Given
        UserModel userModel = new UserModel();
        userModel.setDocument("123456789");
        userModel.setName("John");
        userModel.setLastName("Doe");
        userModel.setEmail("johndoe@gmail.com");
        userModel.setPassword("password");
        userModel.setCellphone("555-5555");

        // when & then
        assertThrows(IllegalArgumentException.class, () -> userUseCase.saveUser(userModel));
    }

    @Test
    public void testSaveUserWithNonExistingRole() {
        // Given
        Set<RoleModel> roles = new HashSet<>();
        roles.add(new RoleModel("ROLE_OWNER"));
        UserModel userModel = new UserModel();
        userModel.setDocument("123456789");
        userModel.setName("John");
        userModel.setLastName("Doe");
        userModel.setEmail("johndoe@gmail.com");
        userModel.setPassword("password");
        userModel.setCellphone("555-5555");
        userModel.setRoles(roles);

        // Mocking
        when(rolePersistencePort.findAllByNameIn(anySet())).thenReturn(Collections.emptyList());

        // When & then
        assertThrows(NoDataFoundException.class, () -> userUseCase.saveUser(userModel));
    }
}
