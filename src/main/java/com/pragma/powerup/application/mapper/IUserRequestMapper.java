package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.request.SaveUserRequestDto;
import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IUserRequestMapper {
    @Mapping(target = "roles", source = "roles", qualifiedByName = "mapRoles")
    UserModel toUserModel(SaveUserRequestDto saveUserRequestDto);

    @Named("mapRoles")
    default Set<RoleModel> mapRoles(Set<String> roles) {
        return roles.stream()
                .map(roleName -> {
                    RoleModel roleModel = new RoleModel();
                    roleModel.setName(roleName);
                    return roleModel;
                })
                .collect(Collectors.toSet());
    }
}
