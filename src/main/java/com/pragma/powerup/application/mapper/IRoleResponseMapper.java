package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.response.RoleResponseDto;
import com.pragma.powerup.domain.model.RoleModel;
import org.mapstruct.*;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRoleResponseMapper {
    public RoleResponseDto toResponse(RoleModel roleModel);
    List<RoleResponseDto> toResponseList(Set<RoleModel> roleModelList);

    RoleModel toModel(RoleResponseDto roleResponseDto);

}
