package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.SaveUserRequestDto;

import java.util.Optional;

public interface IUserHandler {
    void saveUser(SaveUserRequestDto saveUserRequestDto);
}
