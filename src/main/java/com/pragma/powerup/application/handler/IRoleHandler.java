package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.RoleRequestDto;
import com.pragma.powerup.application.dto.response.RoleResponseDto;

import java.util.Collection;
import java.util.List;

public interface IRoleHandler {
    void saveRole(RoleRequestDto roleRequestDto);
    List<RoleResponseDto> getRoleByName(String roleName);

    List<RoleResponseDto> getAllByNameIn(Collection<String> roleNames);
}
