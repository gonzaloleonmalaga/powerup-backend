package com.pragma.powerup.application.dto.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class RoleResponseDto {
    private Long id;
    private String name;
}
