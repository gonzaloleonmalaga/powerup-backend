package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
public class RoleRequestDto {
    private String name;
    public RoleRequestDto(String name) {
        this.name = name;
    }
}
