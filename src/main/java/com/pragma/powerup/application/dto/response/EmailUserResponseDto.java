package com.pragma.powerup.application.dto.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
public class EmailUserResponseDto {
    private String name;
    private String lastName;
    private String document;
    private String cellphone;
    private String email;
    private String password;

    private Set<String> roles;
}
