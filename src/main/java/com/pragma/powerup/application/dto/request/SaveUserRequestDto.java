package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
public class SaveUserRequestDto {
    private String name;
    private String lastName;
    @Pattern(regexp = "^[0-9]+$", message = "Invalid document")
    private String document;
    @Pattern(regexp = "^+?[0-9]{1,13}$", message = "Invalid cellphone")
    private String cellphone;
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$", message = "Invalid email")
    private String email;
    private String password;

    private Set<String> roles;
}
