package com.pragma.powerup.infrastructure.out.jpa.mapper;

import com.pragma.powerup.application.dto.request.SaveUserRequestDto;
import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.infrastructure.out.jpa.entity.RoleEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IUserEntityMapper {
    UserEntity toEntity(UserModel userModel);
    @Mapping(target = "roles", source = "roles", qualifiedByName = "mapRoles")
    UserModel toUserModel(UserEntity userEntity);
    List<UserModel> toUserModelList(List<UserEntity> userEntityList);

    @Named("mapRoles")
    default Set<RoleModel> mapRoles(Set<RoleEntity> roles) {
        return roles.stream()
                .map(roleEntity -> new RoleModel(roleEntity.getId(), roleEntity.getName()))
                .collect(Collectors.toSet());
    }
}
