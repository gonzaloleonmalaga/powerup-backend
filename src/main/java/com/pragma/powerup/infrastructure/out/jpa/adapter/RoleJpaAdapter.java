package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.spi.IRolePersistencePort;
import com.pragma.powerup.infrastructure.out.jpa.entity.RoleEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IRoleEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRoleRepository;
import lombok.RequiredArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class RoleJpaAdapter implements IRolePersistencePort {
    private final IRoleRepository roleRepository;
    private final IRoleEntityMapper roleEntityMapper;

    @Override
    public void saveRole(RoleModel roleModel) {
        RoleEntity roleEntity = roleRepository.save(roleEntityMapper.toEntity(roleModel));
        roleEntityMapper.toRoleModel(roleEntity);
    }

    @Override
    public Set<RoleModel> findRoleByName(String roleName) {
        List<RoleEntity> entityList = roleRepository.findRoleByName(roleName);
        return roleEntityMapper.toRoleModelList(entityList).stream().collect(Collectors.toSet());
    }

    @Override
    public List<RoleModel> findAllByNameIn(Collection<String> roleNames) {
        List<RoleEntity> roleEntities = roleRepository.findAllByNameIn(roleNames);
        return roleEntityMapper.toRoleModelList(roleEntities);
    }
}
