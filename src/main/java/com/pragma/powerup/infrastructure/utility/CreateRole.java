package com.pragma.powerup.infrastructure.utility;

import com.pragma.powerup.application.dto.request.RoleRequestDto;
import com.pragma.powerup.application.handler.impl.RoleHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@Order(1)
public class CreateRole implements CommandLineRunner {
    private final RoleHandler roleHandler;
    @Override
    public void run(String... args) throws Exception {
        if (roleHandler.getRoleByName("ROLE_CLIENT").isEmpty() &&
                roleHandler.getRoleByName("ROLE_CLIENT").isEmpty() &&
                roleHandler.getRoleByName("ROLE_EMPLOYEE").isEmpty() &&
                roleHandler.getRoleByName("ROLE_OWNER").isEmpty()) {
            RoleRequestDto adminRole = new RoleRequestDto("ROLE_ADMIN");
            RoleRequestDto clientRole = new RoleRequestDto("ROLE_CLIENT");
            RoleRequestDto employeeRole = new RoleRequestDto("ROLE_EMPLOYEE");
            RoleRequestDto ownerRole = new RoleRequestDto("ROLE_OWNER");
            roleHandler.saveRole(adminRole);
            roleHandler.saveRole(clientRole);
            roleHandler.saveRole(employeeRole);
            roleHandler.saveRole(ownerRole);
        } else {
            log.info("Roles already created");
        }
    }
}
