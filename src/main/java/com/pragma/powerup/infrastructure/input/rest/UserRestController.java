package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.SaveUserRequestDto;
import com.pragma.powerup.application.handler.IUserHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/plazoleta/user")
@RequiredArgsConstructor
@Slf4j
public class UserRestController {

    private final IUserHandler userHandler;

    @PostMapping("/")
    public ResponseEntity<Void> createUser(@RequestBody SaveUserRequestDto saveUserRequestDto) {
        userHandler.saveUser(saveUserRequestDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
