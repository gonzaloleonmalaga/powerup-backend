package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IRoleServicePort;
import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.spi.IRolePersistencePort;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
public class RoleUseCase implements IRoleServicePort {
    private final IRolePersistencePort rolePersistencePort;

    @Override
    public void saveRole(RoleModel roleModel) {
        rolePersistencePort.saveRole(roleModel);
    }

    @Override
    public Set<RoleModel> getRoleByName(String roleName) {
        return rolePersistencePort.findRoleByName(roleName);
    }

    @Override
    public List<RoleModel> getAllByNameIn(Collection<String> roleNames) {
        return rolePersistencePort.findAllByNameIn(roleNames);
    }
}
