package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IUserServicePort;
import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.spi.IRolePersistencePort;
import com.pragma.powerup.domain.spi.IUserPersistencePort;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class UserUseCase implements IUserServicePort {
    private final IUserPersistencePort userPersistencePort;
    private final IRolePersistencePort rolePersistencePort;

    @Override
    public void saveUser(UserModel userModel) {
        if (userModel.getRoles() == null || userModel.getRoles().isEmpty()) {
            throw new IllegalArgumentException("User must have at least one role");
        }

        Set<RoleModel> roles = new HashSet<>();
        Set<String> roleNames = userModel.getRoles().stream().map(RoleModel::getName).collect(Collectors.toSet());
        List<RoleModel> foundRoles = rolePersistencePort.findAllByNameIn(roleNames);
        if (foundRoles.size() < roleNames.size()) {
            throw new NoDataFoundException();
        }
        roles.addAll(foundRoles);

        userModel.setRoles(roles);
        userPersistencePort.saveUser(userModel);
    }
}
