package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.RoleModel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface IRoleServicePort {

    void saveRole(RoleModel roleModel);
    Set<RoleModel> getRoleByName(String roleName);

    List<RoleModel> getAllByNameIn(Collection<String> roleNames);

}
