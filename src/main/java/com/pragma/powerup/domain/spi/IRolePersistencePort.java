package com.pragma.powerup.domain.spi;

import com.pragma.powerup.domain.model.RoleModel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface IRolePersistencePort {
    void saveRole(RoleModel roleModel);
    Set<RoleModel> findRoleByName(String roleName);

    List<RoleModel> findAllByNameIn(Collection<String> roleNames);
}
