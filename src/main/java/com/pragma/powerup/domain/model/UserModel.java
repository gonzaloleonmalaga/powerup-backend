package com.pragma.powerup.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

    private Long id;
    private String name;
    private String lastName;
    private String document;
    private String cellphone;
    private String email;
    private String password;
    private Set<RoleModel> roles = new HashSet<>();

    public Set<RoleModel> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleModel> roles) {
        this.roles = roles;
    }

    public UserModel(Set<RoleModel> roles) {
        this.roles = roles;
    }
}
